@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown button
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        @foreach($country as $contry)
                                    <a class="dropdown-item" href="{{ route('country.create',['id'=>$contry->id])}}">{{$contry->countryname}}</a>
                                        @endforeach

            </div>

            @foreach($data->posts as $t)
                <div class="container">
                    <div class="card">
                        <div class="card-body col-md-8">

                            <td>
                                <img src="{{url('/storage/'.$t->url)}}" >
                                <h4 class="text-center">Posted By {{$t->user_id}} </h4>
                                <h4  class="text-center"> {{$t->postbody}}</h4>
                        </div>

                    </div>
                    @endforeach
                </div>
        </div>
    </div>
@endsection

