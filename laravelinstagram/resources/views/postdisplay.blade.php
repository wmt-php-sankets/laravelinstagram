@extends('layouts.app')

@section('content')
    <div class="container">
        <button class="btn btn-primary" ><a href="editprofile" class="text-white">UpdateProfile</a> </button>
        <button class="btn btn-primary" ><a href=" {{ route('post.create') }}  " class="text-white">POST</a> </button>
{{--        <button class="btn btn-primary" ><a href=" {{ route(' country.create') }}  " class="text-white">CountryWisepost</a> </button>--}}
        <button class="btn btn-primary" ><a href=" {{ route('image') }}  " class="text-white">image</a> </button>
        <button class="btn btn-primary" ><a href=" {{ route('video') }} " class="text-white">video</a> </button>
    </div>
    <div class="container">
        <div class="col-md-9 offset-2">
    @foreach($post as $post1)
        <div class="container">
       <div class="card">
           <div class="card-body">
               <td>
                   <img src="{{url('/storage/'.$post1->url)}}" >
                   <h4 class="text-center">Posted By {{$post1->user_id}} </h4>
                   <h4  class="text-center"> {{$post1->postbody}}</h4>
           </div>
           <div class="card-footer">
               <div class="text-center">
                   <button class="btn btn-primary text-white"><a href="{{ route('commit.create',$post1->id) }}" class="text-white"> Comment </a> </button>

                   @if(sizeof($post1->user)  == null)
                       <a href="{{url("/like/{$post1->id}")}}" class="btn btn-sm btn-success">like</a>
                   @else
                       <a href="{{url("/unlike/{$post1->id}")}}" class="btn btn-sm btn-danger">Unlike</a>
                   @endif
               @if(Auth::id()==$post1->id)
                  <form action="{{route('post.destroy',$post1->id)}}" method="POST">
                       @csrf
                       @method('DELETE')
                   <button class="btn btn-success">delete</button>
                   </form>
                     @endif
               </div>
           </div>
           </div>
    @endforeach
        </div>
    </div>
    </div>
@endsection
