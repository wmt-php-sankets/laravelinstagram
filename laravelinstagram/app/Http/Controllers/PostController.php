<?php

namespace App\Http\Controllers;

use App\Post;
use App\Like;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $post = Post::all();
        return view('postdisplay',['post'=>$post]);



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('postform');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $image_ext = ['jpg', 'png', 'gif', 'jpeg'];
//        $post = new Post();

        $image = $request->file('url');
        $originalname = uniqid('Img',10) .'.'.$image->getClientOriginalExtension();
        $ext = $image->getClientOriginalExtension();
        $path = $image->storeAs('image_video',$originalname,'public');

//        if (in_array($ext,$image_ext)) {
//            $post->posttype = 'image';
//        }else{
//            $post->posttype = 'video';
//        }
//        $post->url = $path;
//        $post->postbody= $request->postbody;
//        $post->user_id = Auth::id();

//        $post->save();
        $arrayPost = array(
            'url' => $path,
            'postbody' => $request->postbody,
            'user_id' => Auth::id(),
        );
        if (in_array($ext,$image_ext)) {
            $arrayPost['posttype'] = 'image';
            //$post->posttype = 'image';
        }else{
            $arrayPost['posttype'] = 'video';
            //$post->posttype = 'video';
        }

        Post::create($arrayPost);
        return redirect()->route('post.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)

    {
        $post->delete();
       return redirect()->route('post.index');

    }

    public function image(){
        $post = Post::where('posttype','=','image')->get();
        return view('postdisplay',['post'=>$post]);
    }

    public function video(){
        $post = Post::where('posttype','=','video')->get();
        return view('postdisplay',['post'=>$post]);
    }
    public function editprofile(User $user){
          $userid=Auth::id();
          $user=User::find($userid);
        return view('editprofile',compact('user'));
    }
    public function updateprofile(Request $request,$id){

         $user = User::find($id);
        $user->name = $request->name;
        $user->dob = $request->dob;

        $image = $request->file('profile_pic');

        $originalname = $image->getClientOriginalExtension();
        $path = $image->storeAs('profile_pic',$originalname,'public');
        $user->country_id = $request->country_id;
        $user->email = $request->email;
        $user->profile_pic= $path;
        $user->save();
        return redirect()->route('post.index');
    }
    public function like($id){
        $post=self::getPost($id);
        $post->users()->attach(Auth::id());
        return redirect()->route('post.index');
    }
    public function unlike($id){
        $post=self::getPost($id);
        $post->users()->detach(Auth::id());
        return redirect()->route('post.index');
    }

    private function getPost($id){
        return Post::with('users')->find($id);
    }


}
