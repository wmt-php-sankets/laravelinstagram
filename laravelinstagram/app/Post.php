<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'url','postbody','posttype','user_id'
    ];
    public function users()
    {
        return $this->belongsToMany('App\User');
    }
    public function user()
    {
        return $this->belongsToMany(User::class,'post_user');
    }


}
