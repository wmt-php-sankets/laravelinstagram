<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('/post', 'PostController');
Route::resource('/commit', 'CommitController');
Route::resource('/country', 'CountryController');
Route::get('like/{id}','PostController@like')->name('like');
Route::get('unlike/{id}','PostController@unlike')->name('unlike');
Route::get('/editprofile', 'PostController@editprofile');
Route::put('/updateprofile/{id}','PostController@updateprofile')->name('updateprofile');
Route::get('/image','PostController@image')->name('image');
Route::get('/video','PostController@video')->name('video');
